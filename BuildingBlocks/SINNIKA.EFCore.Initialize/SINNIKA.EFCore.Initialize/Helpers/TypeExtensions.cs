﻿using System.Text.Json;

namespace SINNIKA.EFCore.Initialize.Helpers
{
    public static class TypeExtensions
    {
        public static string SetPropertyJson<T>(this T property, T value, out string json)
        {
            property = value;
            json = property == null ? string.Empty : JsonSerializer.Serialize(property);
            return json;
        }

        public static T GetPropertyJson<T>(this string propertyJson, ref T property)
        {
            property ??= string.IsNullOrWhiteSpace(propertyJson) ? default : JsonSerializer.Deserialize<T>(propertyJson);
            return property;
        }
    }
}
