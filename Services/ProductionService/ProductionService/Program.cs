using Microsoft.EntityFrameworkCore;
using ProductionService.Infratructure;
using ProductionService.Infratructure.Repositories;
using ProductionService.Infratructure.Repositories.Imps;
using ProductionService.Infratructure.Services;
using ProductionService.Infratructure.Services.Imps;
using ProductionService.Models;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region service, repository
builder.Services.AddScoped<IJsonRepository, JsonRepository>();
builder.Services.AddScoped<IObjectRepository, ObjectRepository>();

builder.Services.AddScoped<IProductService, ProductService>();

builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#endregion


var connectionString = Environment.GetEnvironmentVariable("connectionstring")
                        ?? configuration.GetConnectionString("CONNECTIONSTRING");
builder.Services.AddDbContext<GTAS_ProductionContext>(option => option.UseSqlServer(connectionString));



var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || true)
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
