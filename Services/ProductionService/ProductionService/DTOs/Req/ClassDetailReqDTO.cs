using System.ComponentModel.DataAnnotations;

namespace ProductionService.DTOs.Req
{
    public class ClassDetailReqDTO
    {
        [Required]
        public int cls_cd { get; set; }
        [Required]
        public string? cls_detail_desc { get; set; }
    }
}