using Microsoft.AspNetCore.Mvc;
using ProductionService.DTOs.Req;
using ProductionService.Infratructure.Services;
using System;

namespace ProductionService.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class ProductionController : ControllerBase
{

    private readonly ILogger<ProductionController> _logger;
    private readonly IProductService _service;

    public ProductionController(ILogger<ProductionController> logger, IProductService service)
    {
        _logger = logger;
        _service = service;
    }
    [HttpGet]
    public async Task<ActionResult<object>> GetFRCustomer()
    {
        try
        {
            var data = await Task.FromResult(_service.GetFRCustomer());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }

    [HttpGet]
    public async Task<ActionResult<object>> CounttblFR_Order()
    {
        try
        {
            var data = await Task.FromResult(_service.CounttblFR_Order());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }

    [HttpGet]
    public async Task<ActionResult<object>> CounttblFR_ProductSWM()
    {
        try
        {
            var data = await Task.FromResult(_service.CounttblFR_ProductSWM());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> DeleteFR_order()
    {
        try
        {
            var data = await Task.FromResult(_service.CounttblFR_Order());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> GetClassDetail([FromBody] ClassDetailReqDTO dataReq)
    {
        try
        {
            var data = await Task.FromResult(_service.GetClassDetail(dataReq));
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> GetStyleSWMByWFXStyle()
    {
        try
        {
            var data = await Task.FromResult(_service.GetStyleSWMByWFXStyle());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> GettblFR_Order()
    {
        try
        {
            var data = await Task.FromResult(_service.GettblFR_Order());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> GettblFR_OrderBylD()
    {
        try
        {
            var data = await Task.FromResult(_service.GettblFR_OrderBylD());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> gettblFR_ProductSWMWfxStyle()
    {
        try
        {
            var data = await Task.FromResult(_service.gettblFR_ProductSWMWfxStyle());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }
    [HttpGet]
    public async Task<ActionResult<object>> pro_Check_lmport_Error()
    {
        try
        {
            var data = await Task.FromResult(_service.pro_Check_lmport_Error());
            if (data == null) return NoContent();
            return Ok(data);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return Task.FromResult(BadRequest(e.Message));
        }
    }

}
