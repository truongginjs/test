namespace ProductionService.Helpers.Enums;
public enum ActiveType
{
    Unknown,
    Find,
    Get,
    AddOrUpdate,
    Delete,
    Save,
    Approve,
    Transfer
}