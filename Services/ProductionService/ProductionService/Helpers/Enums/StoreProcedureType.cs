namespace ProductionService.Helpers.Enums;
public enum StoreProcedureType
{
    Unknown,
    ConfirmtblFR_Order,
    CounttblFR_Order,
    CounttblFR_ProductSWM,
    DeleteFR_order,
    GetClassDetail,
    GetStyleSWMByWFXStyle,
    GettblFR_Order,
    GettblFR_OrderBylD,
    gettblFR_ProductSWMWfxStyle,
    pro_Check_lmport_Error
}