﻿using ProductionService.Models;
using System.Security.Claims;

namespace ProductionService.Helpers.Extensions;
public static class HttpContextAccessorExtensions
    {
        public static User GetUser(this IHttpContextAccessor httpContextAccessor)
        {
            string guidEmpty = Guid.Empty.ToString();
            var result = new User();
            var claimPrincipal = httpContextAccessor.HttpContext.User;
            result.Id = Guid.Parse(claimPrincipal.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? guidEmpty);
            result.UserId = int.Parse(claimPrincipal.FindFirst(ClaimTypes.UserData)?.Value ?? "0");
            result.FullName = claimPrincipal.FindFirst(ClaimTypes.Name)?.Value ?? string.Empty;
            result.Email = claimPrincipal.FindFirst(ClaimTypes.Email)?.Value ?? string.Empty;
            return result;
        }
    }
