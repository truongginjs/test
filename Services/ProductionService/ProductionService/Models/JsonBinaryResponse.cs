namespace ProductionService.Models;

public class JsonBinaryResponse
{
    public byte[] JsbData { get; set; }
}