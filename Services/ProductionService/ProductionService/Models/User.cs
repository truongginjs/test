namespace ProductionService.Models;
public class User
{
    public Guid Id { get; internal set; }
    public int UserId { get; internal set; }
    public string FullName { get; internal set; }
    public string Email { get; internal set; }
}