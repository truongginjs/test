using Microsoft.EntityFrameworkCore;
using ProductionService.Models;
using SINNIKA.EFCore.Initialize;

namespace ProductionService.Infratructure.Repositories.Imps;

public class TestRepository : GenericRepository<Test>, IRepository<Test>
{
    public TestRepository(GTAS_ProductionContext context) : base(context)
    {
    }
}