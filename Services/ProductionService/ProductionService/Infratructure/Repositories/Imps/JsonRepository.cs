using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProductionService.Helpers.Enums;
using ProductionService.Helpers.Extensions;

namespace ProductionService.Infratructure.Repositories.Imps;

public class JsonRepository : IJsonRepository
{
    private readonly GTAS_ProductionContext _context;
    private readonly IHttpContextAccessor _accessor;

    public JsonRepository(GTAS_ProductionContext context, IHttpContextAccessor accessor)
    {
        _context = context;
        _accessor = accessor;
    }

    public T ExecuteData<T>(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, string JSParam = null) where T : class
    {
        try
        {
            var outParam = new SqlParameter()
            {
                ParameterName = "@ReturnMess",
                DbType = DbType.String,
                Size = 255,
                Direction = ParameterDirection.Output
            };

            //_accessor.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues authValue);
            //var token = authValue.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries)[1];

            var data = _accessor.GetUser();
            var userId = data?.UserId;

            string script = $"exec [{storeProcedure}] @Active={{0}}, @jsParam={{1}}, @userId={{2}}, @ReturnMess={{3}} output";
            byte[] jsParam = JSParam.Compress();
            var Active = active.ToString();
            var rs = _context.JsonBinaryResponse.FromSqlRaw(script, Active, jsParam, userId, outParam).AsEnumerable().FirstOrDefault();
            if (rs == null)
            {
                Message_out = (string)(outParam.Value ?? "can't find");
                return null;
            }
            var json = rs.JsbData.Decompress();
            var result = JsonConvert.DeserializeObject<T>(json);
            //var result = JsonSerializer.Deserialize<T>(json);
            if (outParam.Value == DBNull.Value)
                Message_out = "success";
            else
                Message_out = (string)outParam.Value ?? "success";

            return result;
        }
        catch (Exception e)
        {
            Message_out = e.Message;
            Console.WriteLine("loi: ", e.Message);
            return null;
        }
    }

    public T ExecuteFromRawScript<T>(string script, out string Message_out) where T : class
    {
        try
        {
            var rs = _context.JsonBinaryResponse.FromSqlRaw(script).AsEnumerable().FirstOrDefault();
            var json = rs?.JsbData.Decompress() ?? String.Empty;
            var result = System.Text.Json.JsonSerializer.Deserialize<T>(json);
            Message_out = json;
            return result;
        }
        catch (Exception e)
        {
            Message_out = e.Message;
            Console.WriteLine(e.Message);
            return null;
        }
    }

    public string ExecuteGetJson(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, string JSParam = null)
    {
        try
        {
            var outParam = new SqlParameter()
            {
                ParameterName = "@ReturnMess",
                DbType = DbType.String,
                Size = 255,
                Direction = ParameterDirection.Output
            };
            var data = _accessor.GetUser();
            var userId = data?.UserId;

            string script = $"exec [{storeProcedure}] @Active={{0}}, @jsParam={{1}}, @userId={{2}}, @ReturnMess={{3}} output";
            byte[] jsParam = JSParam.Compress();
            var Active = active.ToString();
            var rs = _context.JsonBinaryResponse.FromSqlRaw(script, Active, jsParam, userId, outParam).AsEnumerable().FirstOrDefault();
            if (outParam.Value == DBNull.Value)
                Message_out = "success";
            else
                Message_out = (string)outParam.Value ?? "success";

            var json = rs.JsbData.Decompress();
            return json;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Message_out = e.Message;
            return string.Empty;
        }
    }
    public T ExecuteFromRawScriptToObject<T>(string script, out string Message_out) where T : class
    {
        try
        {
            var rs = _context.JsonResponse.FromSqlRaw(script).AsEnumerable().FirstOrDefault();
            var json = rs?.JsonData ?? String.Empty;
            var result = System.Text.Json.JsonSerializer.Deserialize<T>(json);
            Message_out = json;
            return result;
        }
        catch (Exception e)
        {
            Message_out = e.Message;
            Console.WriteLine(e.Message);
        }
        return null;
    }

    public string ExecuteFromRawScriptToJson(string script, out string Message_out)
    {
        try
        {
            var rs = _context.JsonResponse.FromSqlRaw(script).AsEnumerable().FirstOrDefault();
            var result = rs?.JsonData ?? String.Empty;
            Message_out = result;
            return result;
        }
        catch (Exception e)
        {
            Message_out = e.Message;
            Console.WriteLine(e.Message);
        }
        return String.Empty;
    }
}