using Microsoft.EntityFrameworkCore;
using ProductionService.Helpers.Enums;
using ProductionService.Models;

namespace ProductionService.Infratructure.Repositories;
public class ObjectRepository : IObjectRepository
{
    private readonly GTAS_ProductionContext _context;
    private readonly DbSet<ObjectData> _data;

    public ObjectRepository(GTAS_ProductionContext context)
    {
        _context = context;
        _data = _context.Data;
    }

    public IEnumerable<ObjectData> ExecuteData(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, object parameter)
    {
        string script = $"exec [{storeProcedure}] @Active={{0}}, @jsParam={{1}}, @userId={{2}}, @ReturnMess={{3}} output";
        var Active = active.ToString();
        var result = _context.Data.FromSqlRaw(script).AsEnumerable();
        if (result == null)
        {
            Message_out = "error";
            return null;
        }
        Message_out = "success";

        return result;
    }

    public IEnumerable<ObjectData> ExecuteFromRawScript(string script, out string Message_out)
    {
        var result = _context.Data.FromSqlRaw(script).AsEnumerable();
        if (result == null)
        {
            Message_out = "error";
            return null; 
        }
        Message_out = "success";

        return result;
    }
}