using ProductionService.Helpers.Enums;
using ProductionService.Models;

namespace ProductionService.Infratructure.Repositories;
public interface IObjectRepository
{
    IEnumerable<ObjectData> ExecuteData(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, object parameter);
    IEnumerable<ObjectData> ExecuteFromRawScript(string script, out string Message_out);

}