using ProductionService.Models;
using SINNIKA.EFCore.Initialize;

namespace ProductionService.Infratructure.Repositories;

public interface ITestRepository: IRepository<Test>
{

}