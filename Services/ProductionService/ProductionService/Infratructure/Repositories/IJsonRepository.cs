using ProductionService.Helpers.Enums;

namespace ProductionService.Infratructure.Repositories;

public interface IJsonRepository
{
    T ExecuteData<T>(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, string JSParam) where T : class;
    T ExecuteFromRawScript<T>(string script, out string Message_out) where T : class;
    string ExecuteGetJson(StoreProcedureType storeProcedure, ActiveType active, out string Message_out, string JSParam);

    T ExecuteFromRawScriptToObject<T>(string script, out string Message_out) where T : class;
    string ExecuteFromRawScriptToJson(string script, out string Message_out);

}