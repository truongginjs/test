using Microsoft.EntityFrameworkCore;
using ProductionService.Models;

namespace ProductionService.Infratructure;

public class GTAS_ProductionContext : DbContext
{
    public virtual DbSet<ObjectData> Data { get; set; }
    public virtual DbSet<JsonBinaryResponse> JsonBinaryResponse { get; set; }
    public virtual DbSet<JsonResponse> JsonResponse { get; set; }
    public GTAS_ProductionContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ObjectData>().HasNoKey().ToView(null);
        modelBuilder.Entity<JsonBinaryResponse>().HasNoKey().ToView(null);
        modelBuilder.Entity<JsonResponse>().HasNoKey().ToView(null);
    }
}
