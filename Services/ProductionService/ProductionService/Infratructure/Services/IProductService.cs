using ProductionService.DTOs.Req;

namespace ProductionService.Infratructure.Services;

public interface IProductService
{
    object ConfirmtblFR_Order();
    object CounttblFR_Order();
    object CounttblFR_ProductSWM();
    object DeleteFR_order();
    object GetClassDetail(ClassDetailReqDTO dataReq);
    object GetFRCustomer();
    object GetStyleSWMByWFXStyle();
    object GettblFR_Order();
    object GettblFR_OrderBylD();
    object gettblFR_ProductSWMWfxStyle();
    object pro_Check_lmport_Error();
}