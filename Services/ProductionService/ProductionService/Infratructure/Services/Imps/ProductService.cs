using Newtonsoft.Json;
using ProductionService.DTOs.Req;
using ProductionService.Helpers.Enums;
using ProductionService.Infratructure.Repositories;
using ProductionService.Infratructure.Repositories.Imps;
using ProductionService.Models;

namespace ProductionService.Infratructure.Services.Imps;

public class ProductService : IProductService
{
    private readonly IJsonRepository _repo;

    public ProductService(IJsonRepository repo)
    {
        _repo = repo;
    }

    public object GetFRCustomer()
    {
        var data = _repo.ExecuteFromRawScriptToObject<object>("exec [GetFRCustomer]", out string message);
        return data;
    }
    
    public object ConfirmtblFR_Order ()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.ConfirmtblFR_Order, ActiveType.Unknown, out string message, null);
        return data;
    }



    public object CounttblFR_Order()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.CounttblFR_Order, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object CounttblFR_ProductSWM()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.CounttblFR_ProductSWM, ActiveType.Unknown, out string message, null); 
        return data;
    }

    public object DeleteFR_order()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.DeleteFR_order, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object GetClassDetail(ClassDetailReqDTO dataReq)
    {
        var jsParam = JsonConvert.SerializeObject(dataReq);
        var data = _repo.ExecuteData<object>(StoreProcedureType.GetClassDetail, ActiveType.Unknown, out string message, jsParam);
        return data;
    }


    public object GetStyleSWMByWFXStyle()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.GetStyleSWMByWFXStyle, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object GettblFR_Order()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.GettblFR_Order, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object GettblFR_OrderBylD()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.GettblFR_OrderBylD, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object gettblFR_ProductSWMWfxStyle()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.gettblFR_ProductSWMWfxStyle, ActiveType.Unknown, out string message, null);
        return data;
    }

    public object pro_Check_lmport_Error()
    {
        var data = _repo.ExecuteData<object>(StoreProcedureType.pro_Check_lmport_Error, ActiveType.Unknown, out string message, null);
        return data;
    }
}