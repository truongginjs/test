# GTAS PRODUCTION

## Git command:
- commit
```sh
git checkout {your branch}
git add .
git commit -m "message"
git push
```
- merge to branch
```sh
git checkout {target branch}
git merge {source branch}
git push
git checkout {your branch}
```

## Docker
- docker build
Production service
```sh
docker build -f ".\Services\ProductionService\ProductionService\Dockerfile" --force-rm -t productionservice:{tag} --target base  --label "create-by=nmtruong" --label "project-name=ProductionService" "." 
```

- docker run from images
```sh
docker run -d  -e "ASPNETCORE_ENVIRONMENT=Production" -e "CONNECTIONSTRING=" -p {target port}:80 --name  productionservice:{tag}
```
- docker compose
```sh
docker-compose up
```